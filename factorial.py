"""
@Author Marco A. Gallegos
@Date   2020/12/30
@Upate  2020/12/30
@Description
    Función que reciba un arreglo de números enteros, cada número recibido deberán
    tener sólo valores del 1 al 18, la función deberá regresar otro arreglo con los factoriales de cada
    uno de los números recibidos
"""


def factorial(num:int)-> int:
    """funcion para determinar el factorial de un numero de forma recursiva

    Args:
        num (int): numero para determinar su factoriales

    Returns:
        int: factorial de numero
    """
    if type(num) != int:
        return None
    if num == 1:
        return num
    else:
        return num * factorial(num-1)



def factorial_array(numeros:list)->list:
    """Funcion que determina el factorial de los numero en un arreglo

    Args:
        numeros (list): lista de numeros

    Raises:
        Exception: excepcion cuando no es un numero en rango

    Returns:
        list: lista de factoriales
    """
    factoriales = list()
    for numero in numeros:
        factorial_actual = factorial(numero)
        if type(numero) == int and numero > 0 and numero < 19 and factorial_actual:
            factoriales.append(factorial_actual)
        else:
            raise Exception("los valores deben ser enteros entre 1 y 18")
    return factoriales

if __name__ == '__main__':
    print(factorial_array([2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]))
