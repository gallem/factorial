# Factorial

Realizar una función que reciba un arreglo de números enteros, cada número recibido deberán
tener sólo valores del 1 al 18, la función deberá regresar otro arreglo con los factoriales de cada
uno de los números recibidos. Por ejemplo, si el arreglo contiene los valores [4, 2], tu función
deberá regresar otro arreglo con los valores [24, 2]. Ya que el factorial de 4 es (4 * 3 * 2 * 1) = 24 y
el factorial de 2 es (2 * 1) = 2