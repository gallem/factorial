/**
 * @Author Marco A. Gallegos
 * @Date   2020/12/30
 * @Upate  2020/12/30
 * @Description
 *  Desarrollar una función que reciba un arreglo de números enteros, 
 * cada número recibido deberán tener sólo valores del 1 al 18, 
 * la función deberá regresar otro arreglo con los factoriales de cada uno de los números recibidos
 * sin librerias
 */


/**
 * Recursive funtion to determinate the factorial of a number
 * @param {int} numero 
 * @returns {int} factorial
 */
function factorial(numero){
   if(numero == 1)
      return numero;
   return numero * factorial(numero-1);
}


/**
 * Function to return the factorial of all the numbers between 0 and 19
 * given as parameter or null if a element is another data type
 * @param {list(int)} lista_numeros 
 */
function factorial_array(lista_numeros){
   let support_array = lista_numeros.filter(numero => {
      if(numero>0 && numero<19 && typeof(numero) == "number" && numero % 1 == 0){
         return numero;
      }
   })

   if (support_array.length != lista_numeros.length){
      return null;
   }else{
      let factoriales = lista_numeros.map(numero => {
         let num = factorial(numero);
         if(num){
            return num;
         }
      })

      //console.log(factoriales)
      return factoriales;
   }
   return null;
}


let factorials = factorial_array([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18])
console.table(factorials)